import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                /*2*/
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'Inthira Sakjaroen',
                    textDirection: TextDirection.ltr,
                    style: TextStyle(
                      fontSize: 24,
                      color: Colors.black87,
                    ),
                  ),
                ),
                Text(
                  'Programmer',
                  style: TextStyle(
                    color: Colors.blueGrey[700],
                    fontSize: 20,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    Widget personalprofile_section = Container(
      margin: const EdgeInsets.fromLTRB(10, 32, 10, 32),
      child: Column(children: [
        Container(
          color: Colors.indigo[900],
          height: 40,
          width: 1600,
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Row(
            children: [
              Icon(
                Icons.person_rounded,
                size: 20.0,
                color: Colors.grey[350],
              ),
              Text('   PERSONAL PROFILE',
                  style: TextStyle(
                    color: Colors.grey[50],
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ))
            ],
          ),
        ),
      ]),
    );

    Widget personalprofileText = Container(
        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            'Age                       :    22 years old',
            style: TextStyle(
              color: Colors.grey[900],
              fontSize: 16,
            ),
          ),
          Text(
            'Gender                 :    Female',
            style: TextStyle(
              color: Colors.grey[900],
              fontSize: 16,
            ),
          ),
          Text(
            'Date of birth        :    4 July 1999',
            style: TextStyle(
              color: Colors.grey[900],
              fontSize: 16,
            ),
          ),
          Text(
            'Nationality           :    Thai',
            style: TextStyle(
              color: Colors.grey[900],
              fontSize: 16,
            ),
          ),
          Text(
            'Marital Status     :    Single',
            style: TextStyle(
              color: Colors.grey[900],
              fontSize: 16,
            ),
          ),
        ]));

    Widget education_section = Container(
      margin: const EdgeInsets.fromLTRB(10, 32, 10, 32),
      child: Column(children: [
        Container(
          color: Colors.indigo[900],
          height: 40,
          width: 1600,
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Row(
            children: [
              Icon(
                Icons.school_rounded,
                size: 20.0,
                color: Colors.grey[350],
              ),
              Text('    EDUCATION',
                  style: TextStyle(
                    color: Colors.grey[50],
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ))
            ],
          ),
        ),
      ]),
    );
    Widget educationText = Container(
        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
        child: Column(
          children: [
            Row(
              children: [
                Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 30, 0),
                    height: 50,
                    width: 100,
                    child: Text('2018 - Present',
                        style: TextStyle(
                          fontSize: 16,
                        ))),
                Container(
                  height: 50,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Computer Science – in progress '),
                        Text('Faculty of Information Science,'),
                        Text('Burapha University')
                      ]),
                )
              ],
            ),
            Row(
              children: [
                Container(
                    margin: const EdgeInsets.fromLTRB(0, 0, 30, 0),
                    height: 50,
                    width: 100,
                    child: Text('2016 - 2018')),
                Container(
                    height: 50,
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('High school certificate'),
                          Text('Assumtion Collage Nakhonratchasima School')
                        ]))
              ],
            ),
          ],
        ));

    Widget profile_section = Container(
      margin: const EdgeInsets.fromLTRB(10, 32, 10, 32),
      child: Column(children: [
        Container(
          color: Colors.indigo[900],
          height: 40,
          width: 1600,
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Row(
            children: [
              Icon(
                Icons.account_circle_sharp,
                size: 20.0,
                color: Colors.grey[350],
              ),
              Text('    PROFILE',
                  style: TextStyle(
                    color: Colors.grey[50],
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ))
            ],
          ),
        ),
      ]),
    );
    Widget profileText = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
          'I study programming principles or algorithms, '
          'studying how to write and design software or programs to write,'
          'UX/Ul design, programming languages, etc.'
          'I look to work position as a programmer to further'
          'my knowledge in the IT sector and utilize my skills.',
          softWrap: true,
          style: TextStyle(
            fontSize: 16,
          )),
    );
    Widget skill_section = Container(
      margin: const EdgeInsets.fromLTRB(10, 32, 10, 32),
      child: Column(children: [
        Container(
          color: Colors.indigo[900],
          height: 40,
          width: 1600,
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Row(
            children: [
              Icon(
                Icons.star_rounded,
                size: 20.0,
                color: Colors.grey[350],
              ),
              Text('    SKILLS',
                  style: TextStyle(
                    color: Colors.grey[50],
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ))
            ],
          ),
        ),
      ]),
    );
    Widget skillsSection = Container(
        child: Column(
      children: [
        Row(
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                '../image/java.png',
                width: 60,
                height: 60,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                '../image/eclipse.jpg',
                width: 60,
                height: 60,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                '../image/flutter.png',
                width: 60,
                height: 60,
              ),
            )
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                '../image/vscode.png',
                width: 60,
                height: 60,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                '../image/netbean.png',
                width: 60,
                height: 60,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                '../image/vue.png',
                width: 60,
                height: 60,
              ),
            )
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                '../image/js.png',
                width: 60,
                height: 60,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                '../image/css.png',
                width: 60,
                height: 60,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                '../image/html.png',
                width: 60,
                height: 60,
              ),
            )
          ],
        ),
        Row(
          children: [
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                '../image/python.png',
                width: 60,
                height: 60,
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
              child: Image.asset(
                '../image/mysql.png',
                width: 60,
                height: 60,
              ),
            ),
          ],
        )
      ],
    ));

    return MaterialApp(
        title: 'My Resume',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('My Resume'),
          ),
          backgroundColor: Color(0xffe8eaf6),
          body: ListView(children: [
            Image.asset(
              '../image/profile.jpg',
              width: 200,
              height: 400,
            ),
            titleSection,
            personalprofile_section,
            personalprofileText,
            profile_section,
            profileText,
            education_section,
            educationText,
            skill_section,
            skillsSection,
          ]),
        ));
  }
}
